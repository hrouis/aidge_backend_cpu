![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/pipeline.svg?ignore_skipped=true) ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)

# Aidge CPU library

You can find in this folder the library that implements the CPU operators. <br>
Those operators can be used on any machine with an Linux OS.

So far be sure to have the correct requirements to use this library
- GCC
- Make
- CMake
- aidge_core
- Python (optional, if you have no intend to use this library in python with pybind)

## Pip installation

You will need to install first the ``aidge_core`` library before installing ``aidge_backend_cpu``.

If you have set a custom install path for the ``aidge_core`` library, make sure to use the same one here.

Then run in your python environnement :
``` bash
pip install . -v
```

## Standard C++ Compilation

You will need to compile first the Core library before compiling the CPU one.
The makefile is designed to do it for you.

To only compile the CPU library, run
```
make cpu_only
```

To compile the CPU library + the associated unitary tests, run
```
make cpu_tests
```

To compile the CPU library with the python binding, run
```
make cpu_with_pybind
```
Important: this command can also be run with `make`.


To compile the CPU library with the python binding + the associated unitary tests, run
```
make cpu_with_pybind_tests
```
