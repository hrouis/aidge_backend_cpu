/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_H_
#define AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ConvDepthWise.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class ConvDepthWise_Op;

// compute kernel registry for forward and backward
class ConvDepthWiseImpl2DForward_cpu
    : public Registrable<ConvDepthWiseImpl2DForward_cpu,
                         std::tuple<DataType, DataType, DataType, DataType>,
                         void(const ConvDepthWise_Op<2>::Attrs &, const std::array<DimSize_t, 4> &, const void *,
                              const void *, const void *, void *)> {};
class ConvDepthWiseImpl2DBackward_cpu
    : public Registrable<ConvDepthWiseImpl2DBackward_cpu,
                         std::tuple<DataType, DataType, DataType, DataType>,
                         void(const ConvDepthWise_Op<2>::Attrs &, const std::array<DimSize_t, 4> &, const void *,
                              const void *, const void *, void *)> {};

class ConvDepthWiseImpl2D_cpu : public OperatorImpl {
public:
    ConvDepthWiseImpl2D_cpu(const ConvDepthWise_Op<2> &op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<ConvDepthWiseImpl2D_cpu> create(const ConvDepthWise_Op<2> &op) {
        return std::make_unique<ConvDepthWiseImpl2D_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to ConvDepthWise_Op<2> implementation registry
static Registrar<ConvDepthWise_Op<2>> registrarConvDepthWiseImpl2D_cpu("cpu", Aidge::ConvDepthWiseImpl2D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_H_ */
