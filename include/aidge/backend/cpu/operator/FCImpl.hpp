/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FCIMPL_H_
#define AIDGE_CPU_OPERATOR_FCIMPL_H_

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/FC.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>
#include <array>

namespace Aidge {
// class FC_Op;

// compute kernel registry for forward and backward
class FCImplForward_cpu : public Registrable<FCImplForward_cpu,
                                             std::tuple<DataType,
                                                        DataType,
                                                        DataType,
                                                        DataType>,
                                             void(const FC_Op::Attrs&,
                                                  const DimSize_t,
                                                  const DimSize_t,
                                                  const void *,
                                                  const void *,
                                                  const void *,
                                                  void *)> {};
class FCImplBackward_cpu : public Registrable<FCImplBackward_cpu,
                                              std::tuple<DataType,
                                                         DataType,
                                                         DataType,
                                                         DataType>,
                                              void(const FC_Op::Attrs&,
                                              const DimSize_t,
                                              const DimSize_t,
                                              const void *,
                                              const void *,
                                              const void *,
                                              void *,
                                              void *,
                                              void *)> {};

class FCImpl_cpu : public OperatorImpl {
public:
    FCImpl_cpu(const FC_Op &op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<FCImpl_cpu> create(const FC_Op &op) {
        return std::make_unique<FCImpl_cpu>(op);
    }

    void forward() override final;
    void backward() override final;
};

namespace {
static Registrar<FC_Op> registrarFCImpl_cpu("cpu", Aidge::FCImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FCIMPL_H_ */
