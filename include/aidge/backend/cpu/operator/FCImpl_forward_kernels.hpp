/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FCIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_FCIMPL_FORWARD_KERNEL_H_

#include <algorithm>

#include "aidge/backend/cpu/operator/FCImpl.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
// template <class I, class W, class B, class O>
// void FCImpl_cpu_forward_kernel(const FC_Op::Attrs& attrs, const std::array<DimSize_t, 4>& dims,
//                                    const void* input_, const void* weights_, const void* biases_, void* output_) {
//     // FIXME: missing FC attributes as arguments
//     const I* input = static_cast<const I*>(input_);
//     const W* weights = static_cast<const W*>(weights_);
//     const B* biases = static_cast<const B*>(biases_);
//     O* output = static_cast<O*>(output_);

//     for (std::size_t outIdx = 0; outIdx < std::get<0>(attrs); ++outIdx) {
//         std::size_t oIndex = outIdx * dims[3];
//         const B bias = std::get<1>(attrs) ? B(0) : biases[outIdx];
//         for (std::size_t batch = 0; batch < dims[3]; ++batch) {
//             output[oIndex + batch] = bias;
//         }
//     }

//     for (std::size_t ix = 0; ix < dims[0]; ++ix) {
//         for (std::size_t iy = 0; iy < dims[1]; ++iy) {
//             for (std::size_t inCh = 0; inCh < dims[2]; ++inCh) {
//                 const std::size_t iIndex = dims[3] * (inCh + dims[2] * (iy + dims[1] * ix));
//                 for (std::size_t outCh = 0; outCh < std::get<0>(attrs); ++outCh) {
//                     const std::size_t oIndex = dims[3] * outCh;
//                     const std::size_t wIndex = (inCh + dims[2] * (iy + dims[1] * ix)) * std::get<0>(attrs) +
//                                           outCh;  // (iIndex*std::get<0>(attrs) + oIndex)/dims[3];
//                     for (std::size_t batch = 0; batch < dims[3]; ++batch) {
//                         output[oIndex + batch] += weights[wIndex] * input[iIndex + batch];
//                     }
//                 }
//             }
//         }
//     }
// }

// template <class I, class W, class B, class O>
// void FCImpl_cpu_forward_kernel(const FC_Op::Attrs& attrs, const std::array<DimSize_t, 2>& dims,
//                                    const void* input_, const void* weights_, const void* biases_, void* output_) {
//     // FIXME: missing FC attributes as arguments
//     const I* input = static_cast<const I*>(input_);
//     const W* weights = static_cast<const W*>(weights_);
//     const B* biases = static_cast<const B*>(biases_);
//     O* output = static_cast<O*>(output_);

//     // let's have I.dims() = [N, C, H, W] instead of [H, W, C, N]

//     for (std::size_t outIdx = 0; outIdx < std::get<0>(attrs); ++outIdx) {
//         std::size_t oIndex = outIdx * dims[0];
//         const B bias = std::get<1>(attrs) ? B(0) : biases[outIdx];
//         for (std::size_t batch = 0; batch < dims[0]; ++batch) {
//             output[oIndex + batch] = bias;
//         }
//     }

//     for (std::size_t batch = 0; batch < dims[0]; ++batch) {
//         const std::size_t oIndex = dims[1] * batch;
//         for (std::size_t i = 0; i < dims[1]; ++i) {
//             for (std::size_t outCh = 0; outCh < std::get<0>(attrs); ++outCh) {
//                 std::size_t wIndex = i * std::get<0>(attrs) + outCh;  // (iIndex*std::get<0>(attrs) + oIndex)/dims[3];
//                 output[oIndex + outCh] += weights[wIndex] * input[i + batch];
//             }
//         }
//     }
// }

template <class I, class W, class B, class O>
void FCImpl_cpu_forward_kernel(const FC_Op::Attrs& attrs, const DimSize_t batchSize, const DimSize_t oneInputSize,
                                   const void* input_, const void* weights_, const void* biases_, void* output_) {
    // FIXME: missing FC attributes as arguments
    const I* input = static_cast<const I*>(input_);
    const W* weights = static_cast<const W*>(weights_);
    const B* biases = static_cast<const B*>(biases_);
    O* output = static_cast<O*>(output_);

    if (std::get<1>(attrs)) {
        std::fill(output, output+(batchSize*std::get<0>(attrs)), B(0));
    }
    else {
        for (std::size_t batch = 0; batch < batchSize; ++batch) {
            std::copy(biases, biases+std::get<0>(attrs), output+(batch*std::get<0>(attrs)));
        }
    }

    for (std::size_t batch = 0; batch < batchSize; ++batch) {
        for (std::size_t out = 0; out < std::get<0>(attrs); ++out) {
            output[out + batch*std::get<0>(attrs)] = std::inner_product(input + batch*oneInputSize,
                                                        input + (batch + 1)*oneInputSize,
                                                        weights + out*oneInputSize,
                                                        output[out + batch*std::get<0>(attrs)]);
        }
    }
}


namespace {
static Registrar<FCImplForward_cpu> registrarFCImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::FCImpl_cpu_forward_kernel<float, float, float, float>);
static Registrar<FCImplForward_cpu> registrarFCImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::FCImpl_cpu_forward_kernel<int, int, int, int>);
static Registrar<FCImplForward_cpu> registrarFCImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::FCImpl_cpu_forward_kernel<double, double, double, double>);
}  // namespace

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FCIMPL_FORWARD_KERNEL_H_ */
