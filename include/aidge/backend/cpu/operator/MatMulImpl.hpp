/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MATMULIMPL_H_
#define AIDGE_CPU_OPERATOR_MATMULIMPL_H_

#include <array>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/MatMul.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {

class MatMulImplForward_cpu
    : public Registrable<MatMulImplForward_cpu, std::tuple<DataType, DataType>,
                         void(const std::size_t, const std::size_t, const std::size_t,
                              const void *, const void *, void *)> {};
class MatMulImplBackward_cpu
    : public Registrable<MatMulImplBackward_cpu, std::tuple<DataType, DataType>,
                         void(const std::vector<DimSize_t>&, const std::vector<DimSize_t>&,
                              const void *, const void *, void *)> {};

class MatMulImpl_cpu : public OperatorImpl {
public:
    MatMulImpl_cpu(const MatMul_Op &op): OperatorImpl(op, "cpu") {}

    static std::unique_ptr<MatMulImpl_cpu> create(const MatMul_Op &op) {
        return std::make_unique<MatMulImpl_cpu>(op);
    }

    void forward() override;
};

namespace {
static Registrar<MatMul_Op> registrarMatMulImpl_cpu("cpu", Aidge::MatMulImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MATMULIMPL_H_ */
