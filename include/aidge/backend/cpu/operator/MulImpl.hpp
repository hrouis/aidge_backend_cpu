/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MULIMPL_H_
#define AIDGE_CPU_OPERATOR_MULIMPL_H_

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// class Mul_Op;

// compute kernel registry for forward and backward
class MulImplForward_cpu
    : public Registrable<MulImplForward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::vector<std::size_t>&, const std::vector<std::size_t>&, const std::vector<std::size_t>&, const void*, const void*,void*)> {
};
class MulImplBackward_cpu
    : public Registrable<MulImplBackward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::vector<std::size_t>&, const std::vector<std::size_t>&, const std::vector<std::size_t>&, const void*, const void*, void*)> {
};

class MulImpl_cpu : public OperatorImpl {
public:
    MulImpl_cpu(const Mul_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<MulImpl_cpu> create(const Mul_Op& op) {
        return std::make_unique<MulImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
static Registrar<Mul_Op> registrarMulImpl_cpu("cpu", Aidge::MulImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MULIMPL_H_ */
