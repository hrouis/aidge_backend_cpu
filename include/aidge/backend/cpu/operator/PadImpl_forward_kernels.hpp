/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_PADIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_PADIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/PadImpl.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <cmath>
#include <array>
#include <algorithm>

namespace Aidge {
/**
 * @brief Forward kernel for 2D Padding on CPU backend.
 * @tparam I Input data type.
 * @tparam O Output data type.
 * @param attrs tuple of Parameters from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class O>
void PadImpl2D_cpu_forward_kernel(const Pad_Op<2>::Attrs &attrs, const std::array<DimSize_t, 4> &dims,
                                       const void *input_, void *output_)
{
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    const std::size_t oySize = dims[2] + std::get<0>(attrs)[0] + std::get<0>(attrs)[1];
    const std::size_t oxSize = dims[3] + std::get<0>(attrs)[2] + std::get<0>(attrs)[3];

    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < dims[1]; ++ch) {
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2] * dims[3];
            const std::size_t oIndex = (ch + batch*dims[1]) * oxSize * oySize;

            for (unsigned int oy = 0; oy < oySize; ++oy) {
                for (unsigned int ox = 0; ox < oxSize; ++ox) {
                    const std::size_t oIndexFull = oIndex + ox*oySize + oy;

                    O outputValue = std::get<2>(attrs);

                    if (std::get<1>(attrs) == PadBorderType::Constant) {
                        int ix = static_cast<int>(ox) - static_cast<int>(std::get<0>(attrs)[3]);
                        int iy = static_cast<int>(oy) - static_cast<int>(std::get<0>(attrs)[1]);

                        if (ix >= 0  && ix < static_cast<int>(dims[3]) && iy >= 0  && iy < static_cast<int>(dims[2])) {
                            outputValue = input[iIndex + static_cast<std::size_t>(ix)*dims[2] + static_cast<std::size_t>(iy)];
                        }
                    }
                    else if (std::get<1>(attrs) == PadBorderType::Edge) {
                        int ix = std::max(0, std::min(static_cast<int>(dims[3]) - 1, static_cast<int>(ox) - static_cast<int>(std::get<0>(attrs)[3])));
                        int iy = std::max(0, std::min(static_cast<int>(dims[2]) - 1, static_cast<int>(oy) - static_cast<int>(std::get<0>(attrs)[1])));

                        outputValue = input[iIndex + static_cast<std::size_t>(ix)*dims[2] + static_cast<std::size_t>(iy)];
                    }
                    else if (std::get<1>(attrs) == PadBorderType::Reflect) {
                        int ix = static_cast<int>(ox) - static_cast<int>(std::get<0>(attrs)[3]);
                        int iy = static_cast<int>(oy) - static_cast<int>(std::get<0>(attrs)[1]);

                        if (ix < 0)
                            ix = 0 - ix;
                        if (iy < 0)
                            iy = 0 - iy;
                        if (ix >= static_cast<int>(dims[3]))
                            ix = static_cast<int>(dims[3]) - ix;
                        if (iy >= static_cast<int>(dims[2]))
                            iy = static_cast<int>(dims[2]) - iy;

                        outputValue = input[iIndex + static_cast<std::size_t>(ix)*dims[2] + static_cast<std::size_t>(iy)];
                    }
                    else if (std::get<1>(attrs) == PadBorderType::Wrap) {
                        int ix = (static_cast<int>(dims[3]) + static_cast<int>(ox) - static_cast<int>(std::get<0>(attrs)[3])) % static_cast<int>(dims[3]);
                        int iy = (static_cast<int>(dims[2]) + static_cast<int>(oy) - static_cast<int>(std::get<0>(attrs)[1])) % static_cast<int>(dims[2]);

                        outputValue = input[iIndex + static_cast<std::size_t>(ix)*dims[2] + static_cast<std::size_t>(iy)];
                    }

                    output[oIndexFull] = outputValue;
                }
            }
        }
    }
}

namespace {
static Registrar<PadImpl2DForward_cpu> registrarPadImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32},
        Aidge::PadImpl2D_cpu_forward_kernel<float, float>);
static Registrar<PadImpl2DForward_cpu> registrarPadImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32},
        Aidge::PadImpl2D_cpu_forward_kernel<int, int>);
static Registrar<PadImpl2DForward_cpu> registrarPadImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64},
        Aidge::PadImpl2D_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_PADIMPL_FORWARD_KERNEL_H_ */
