/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_RELUIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_RELUIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/ReLUImpl.hpp"

namespace Aidge {
template <class I, class O>
void ReLUImpl_cpu_forward_kernel(std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

//#pragma omp parallel for if (inputLenght > 1024)
    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = input[i] > 0 ? input[i] : 0;
    }
}

namespace {
static Registrar<ReLUImplForward_cpu> registrarReLUImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::ReLUImpl_cpu_forward_kernel<float, float>);
static Registrar<ReLUImplForward_cpu> registrarReLUImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::ReLUImpl_cpu_forward_kernel<int, int>);
static Registrar<ReLUImplForward_cpu> registrarReLUImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::ReLUImpl_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_RELUIMPL_FORWARD_KERNEL_H_ */
