/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_
#define AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// class ReduceMean_Op;

// Every DIM
class ReduceMeanImplForward_cpu
    : public Registrable<ReduceMeanImplForward_cpu,
                         std::tuple<DataType, DataType>,
                         void(const ReduceMean_Op::Attrs &, const std::vector<DimSize_t>&, const void *, void *)> {};
class ReduceMeanImpl1DBackward_cpu
    : public Registrable<ReduceMeanImpl1DBackward_cpu,
                         std::tuple<DataType, DataType>,
                         void(const ReduceMean_Op::Attrs &, const std::vector<DimSize_t>&, const void *,  void *)> {};

class ReduceMeanImpl_cpu : public OperatorImpl {
   public:
    ReduceMeanImpl_cpu(const ReduceMean_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<ReduceMeanImpl_cpu> create(const ReduceMean_Op &op) {
        return std::make_unique<ReduceMeanImpl_cpu>(op);
    }

   public:
    void forward() override;
};

// // compute kernel registry for forward and backward
// // DIM 1
// class ReduceMeanImpl1DForward_cpu
//     : public Registrable<ReduceMeanImpl1DForward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<1>::Attrs &, const std::vector<DimSize_t>&, const void *, void *)> {};
// class ReduceMeanImpl1DBackward_cpu
//     : public Registrable<ReduceMeanImpl1DBackward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<1>::Attrs &, const std::vector<DimSize_t>&, const void *,  void *)> {};

// // DIM 2
// class ReduceMeanImpl2DForward_cpu
//     : public Registrable<ReduceMeanImpl2DForward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<2>::Attrs &, const std::vector<DimSize_t>&, const void *, void *)> {};
// class ReduceMeanImpl2DBackward_cpu
//     : public Registrable<ReduceMeanImpl2DBackward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<2>::Attrs &, const std::vector<DimSize_t>&, const void *,  void *)> {};
// // DIM 3
// class ReduceMeanImpl3DForward_cpu
//     : public Registrable<ReduceMeanImpl3DForward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<3>::Attrs &, const std::vector<DimSize_t>&, const void *, void *)> {};
// class ReduceMeanImpl3DBackward_cpu
//     : public Registrable<ReduceMeanImpl3DBackward_cpu,
//                          std::tuple<DataType, DataType>,
//                          void(const ReduceMean_Op<3>::Attrs &, const std::vector<DimSize_t>&, const void *, void *)> {};

// class ReduceMeanImpl1D_cpu : public OperatorImpl {
//    public:
//     ReduceMeanImpl1D_cpu(const ReduceMean_Op<1>& op) : OperatorImpl(op, "cpu") {}

//     static std::unique_ptr<ReduceMeanImpl1D_cpu> create(const ReduceMean_Op<1> &op) {
//         return std::make_unique<ReduceMeanImpl1D_cpu>(op);
//     }

//    public:
//     void forward() override;
// };

// class ReduceMeanImpl2D_cpu : public OperatorImpl {
//    public:
//     ReduceMeanImpl2D_cpu(const ReduceMean_Op<2>& op) : OperatorImpl(op, "cpu") {}

//     static std::unique_ptr<ReduceMeanImpl2D_cpu> create(const ReduceMean_Op<2> &op) {
//         return std::make_unique<ReduceMeanImpl2D_cpu>(op);
//     }

//    public:
//     void forward() override;
// };

// class ReduceMeanImpl3D_cpu : public OperatorImpl {
//    public:
//     ReduceMeanImpl3D_cpu(const ReduceMean_Op<3>& op) : OperatorImpl(op, "cpu") {}

//     static std::unique_ptr<ReduceMeanImpl3D_cpu> create(const ReduceMean_Op<3> &op) {
//         return std::make_unique<ReduceMeanImpl3D_cpu>(op);
//     }

//    public:
//     void forward() override;
// };
namespace {
// add cpu backend to ReduceMean_Op<2> implementation registry
static Registrar<ReduceMean_Op> registrarReduceMeanImpl_cpu("cpu", Aidge::ReduceMeanImpl_cpu::create);
// static Registrar<ReduceMean_Op<1>> registrarReduceMeanImpl1D_cpu("cpu", Aidge::ReduceMeanImpl1D_cpu::create);
// static Registrar<ReduceMean_Op<2>> registrarReduceMeanImpl2D_cpu("cpu", Aidge::ReduceMeanImpl2D_cpu::create);
// static Registrar<ReduceMean_Op<3>> registrarReduceMeanImpl3D_cpu("cpu", Aidge::ReduceMeanImpl3D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_H_ */
