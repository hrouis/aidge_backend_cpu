/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef __AIDGE_CPU_OPERATOR_ScalingIMPL_H__
#define __AIDGE_CPU_OPERATOR_ScalingIMPL_H__

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Scaling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>
#include <array>

namespace Aidge {
// class Scaling_Op;

// compute kernel registry for forward and backward
class ScalingImplForward_cpu
    : public Registrable<ScalingImplForward_cpu, std::tuple<DataType, DataType>, void(const Scaling_Op::Attrs&, std::size_t, const void*, void*)> {
};
class ScalingImplBackward_cpu
    : public Registrable<ScalingImplBackward_cpu, std::tuple<DataType, DataType>, void(const Scaling_Op::Attrs&, std::size_t, const void*, void*)> {
};

class ScalingImpl_cpu : public OperatorImpl {
public:
    ScalingImpl_cpu(const Scaling_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<ScalingImpl_cpu> create(const Scaling_Op& op) {
        return std::make_unique<ScalingImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
static Registrar<Scaling_Op> registrarScalingImpl_cpu("cpu", Aidge::ScalingImpl_cpu::create);
}
}  // namespace Aidge

#endif /* __AIDGE_CPU_OPERATOR_ScalingIMPL_H__ */