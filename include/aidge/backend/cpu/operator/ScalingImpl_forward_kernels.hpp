/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SCALINGIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_SCALINGIMPL_FORWARD_KERNEL_H_

#include <cmath>
#include <cstddef>
#include "aidge/utils/Registrar.hpp"
#include "aidge/backend/cpu/operator/ScalingImpl.hpp"

//TODO : improve propagate, n2d2 :
/*
template<typename T>
void N2D2::floatingPointScaling_propagate(const Tensor<T>& input, Tensor<T>& output,
                                          std::size_t batchSize, std::size_t nbChannels,
                                          std::size_t height, std::size_t width,
                                          bool isClipped,
                                          const std::vector<Float_T>& clippingFactorPerChannel,
                                          const std::vector<Float_T>& scalingFactorPerChannel,
                                          std::size_t quantizedNbBits, bool isOutputUnsigned)
{
    std::size_t index = 0;
    for (std::size_t batch = 0; batch < batchSize; batch++) {
        for(std::size_t ch = 0; ch < nbChannels; ch++) {
            for(std::size_t y = 0; y < height; y++) {
                for(std::size_t x = 0; x < width; x++) {

                    T res = isClipped ? Clip(input(index), clippingFactorPerChannel[ch])
                                    : input(index);
                    res = Scale(res, scalingFactorPerChannel[ch]);

                    if(quantizedNbBits > 0) {
                        res = saturate(std::round(res), quantizedNbBits, isOutputUnsigned);
                    }
                    output(index) = (T) res;
                    index++;
                }
            }
        }
    }
}
*/


namespace Aidge {

template <class O>
const O& clamp(const O& x, const O& min, const O& max)
{
    return (x < min) ? min : (x > max) ? max : x;
}

template<class O>
O saturate(const O value, const std::size_t quantizedNbBits, const bool isOutputUnsigned) {
    // TODO: no assertions in kernel
    assert(quantizedNbBits > 0);

    const O min = isOutputUnsigned ? 0 :
                                  -(1ll << (quantizedNbBits - 1ll));
    const O max = isOutputUnsigned ? (1ll << quantizedNbBits) - 1ll :
                                   (1ll << (quantizedNbBits - 1ll)) - 1ll;

    return clamp(value, min, max);
}

template <class I, class O>
void ScalingImpl_cpu_forward_kernel(const Scaling_Op::Attrs& attrs,
                                     std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
    const I& scalingFactor = static_cast<const I&>(std::get<0>(attrs));
    const std::size_t quantizedNbBits = static_cast<std::size_t>(std::get<1>(attrs));
    const bool isOutputUnsigned = static_cast<bool>(std::get<2>(attrs));

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = input[i] * scalingFactor;

        if(quantizedNbBits > 0) {
                output[i] = saturate(std::round(output[i]), quantizedNbBits, isOutputUnsigned);
        }
    }
}

namespace {
static Registrar<ScalingImplForward_cpu> registrarScalingImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::ScalingImpl_cpu_forward_kernel<float, float>);
static Registrar<ScalingImplForward_cpu> registrarScalingImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::ScalingImpl_cpu_forward_kernel<int, int>);
static Registrar<ScalingImplForward_cpu> registrarScalingImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::ScalingImpl_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SCALINGIMPL_FORWARD_KERNEL_H_ */