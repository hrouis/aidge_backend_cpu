#include <pybind11/pybind11.h>
// Need to call this header to register every impl
#include "aidge/backend/cpu.hpp"

namespace py = pybind11;

namespace Aidge {

void init_cpu_sys_info(py::module& m);

void init_Aidge(py::module& m){
    init_cpu_sys_info(m);
}


PYBIND11_MODULE(aidge_backend_cpu, m) {
    init_Aidge(m);
}
}
