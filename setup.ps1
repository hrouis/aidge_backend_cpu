# Helper setup tool to automatically build aidge_backend_cpu on Windows.

# Requirements
################################################################################
# aidge_core must be installed first in $env:AIDGE_INSTALL_PATH.

# Enable or disable automatic installation of requirements
# Run .\setup.ps1 -install_reqs:$false to disable it
param ([bool]$install_reqs=$true)

if (-not $env:AIDGE_INSTALL_PATH)
{
    Write-Error -Message "AIDGE_INSTALL_PATH environment variable must be set to aidge_core install path." -ErrorAction Stop
}

# 1. Setup environment
################################################################################
if ($install_reqs)
{
    # No additional dependencies
}

# 2. Compile & install aidge_core
################################################################################
$env:CMAKE_PREFIX_PATH=$env:AIDGE_INSTALL_PATH
mkdir -Force build_cpp
mkdir -Force $env:AIDGE_INSTALL_PATH
Set-Location build_cpp
cmake -DCMAKE_INSTALL_PREFIX:PATH=$env:AIDGE_INSTALL_PATH -DCMAKE_BUILD_TYPE=Debug ..
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
cmake --build . -j2
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
cmake --install . --config Debug
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
# Optional: run the unit tests
ctest --output-on-failure
if(!$?) { $lastError = $LASTEXITCODE; Set-Location $PSScriptRoot; Exit $lastError }
Set-Location $PSScriptRoot
