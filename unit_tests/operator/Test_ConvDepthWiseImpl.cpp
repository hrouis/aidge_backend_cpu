/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <memory>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/ConvDepthWise.hpp"

#include "aidge/backend/cpu.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] ConvDepthWise(forward)", "[ConvDepthWise][CPU]") {
    std::shared_ptr<Node> myCDW = ConvDepthWise(4, {3,3}, "mycdw");
    auto op = std::static_pointer_cast<OperatorTensor>(myCDW -> getOperator());
    std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(Array4D<int,4,1,3,3> {
        {
            {{
                {  0,  1,  2},
                {  3,  4,  5},
                {  6,  7,  8}

            }},
            {{
                { 27, 28, 29},
                { 30, 31, 32},
                { 33, 34, 35}

            }},
            {{
                { 54, 55, 56},
                { 57, 58, 59},
                { 60, 61, 62}
            }},
            {{
                { 81, 82, 83},
                { 84, 85, 86},
                { 87, 88, 89}
            }}
        }
    });
    std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(Array1D<int,4> {{7,0,9,0}});
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<int,2,4,5,5> { //NCHW
        {
            {
                {{  0,   1,   2,   3,   4},
                 {  5,   6,   7,   8,   9},
                 { 10,  11,  12,  13,  14},
                 { 15,  16,  17,  18,  19},
                 { 20,  21,  22,  23,  24}},

                {{ 25,  26,  27,  28,  29},
                 { 30,  31,  32,  33,  34},
                 { 35,  36,  37,  38,  39},
                 { 40,  41,  42,  43,  44},
                 { 45,  46,  47,  48,  49}},

                {{ 50,  51,  52,  53,  54},
                 { 55,  56,  57,  58,  59},
                 { 60,  61,  62,  63,  64},
                 { 65,  66,  67,  68,  69},
                 { 70,  71,  72,  73,  74}},

                {{ 75,  76,  77,  78,  79},
                 { 80,  81,  82,  83,  84},
                 { 85,  86,  87,  88,  89},
                 { 90,  91,  92,  93,  94},
                 { 95,  96,  97,  98,  99}}
            },
            {
                {{100, 101, 102, 103, 104},
                 {105, 106, 107, 108, 109},
                 {110, 111, 112, 113, 114},
                 {115, 116, 117, 118, 119},
                 {120, 121, 122, 123, 124}},

                {{125, 126, 127, 128, 129},
                 {130, 131, 132, 133, 134},
                 {135, 136, 137, 138, 139},
                 {140, 141, 142, 143, 144},
                 {145, 146, 147, 148, 149}},

                {{150, 151, 152, 153, 154},
                 {155, 156, 157, 158, 159},
                 {160, 161, 162, 163, 164},
                 {165, 166, 167, 168, 169},
                 {170, 171, 172, 173, 174}},

                {{175, 176, 177, 178, 179},
                 {180, 181, 182, 183, 184},
                 {185, 186, 187, 188, 189},
                 {190, 191, 192, 193, 194},
                 {195, 196, 197, 198, 199}}
            }
        }
    });
    std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(Array4D<int,2,4,3,3> {
        {
            {
                {{   319,    355,    391},
                 {   499,    535,    571},
                 {   679,    715,    751}},

                {{  8745,   9024,   9303},
                 { 10140,  10419,  10698},
                 { 11535,  11814,  12093}},

                {{ 29337,  29859,  30381},
                 { 31947,  32469,  32991},
                 { 34557,  35079,  35601}},

                {{ 62061,  62826,  63591},
                 { 65886,  66651,  67416},
                 { 69711,  70476,  71241}}
            },
            {
                {{  3919,   3955,   3991},
                 {  4099,   4135,   4171},
                 {  4279,   4315,   4351}},

                {{ 36645,  36924,  37203},
                 { 38040,  38319,  38598},
                 { 39435,  39714,  39993}},

                {{ 81537,  82059,  82581},
                 { 84147,  84669,  85191},
                 { 86757,  87279,  87801}},

                {{138561, 139326, 140091},
                 {142386, 143151, 143916},
                 {146211, 146976, 147741}}
            }
        }
    });
    op -> associateInput(0, myInput);
    op -> associateInput(1, myWeights);
    op -> associateInput(2, myBias);
    op->setDataType(DataType::Int32);
    op->setBackend("cpu");
    myCDW -> forward();
    op -> getOutput(0) -> print();
    REQUIRE(*(op -> getOutput(0)) == *myOutput);

    // std::cout << static_cast<Tensor>((*op)["weight"])[0][0][0][0] << std::endl;
}